let JSOQ = require("jsoqs");
let fs = require("fs");
let path = require("path");

let listFiles = async (pathname)=>{
    return fs.readdirSync(pathname);
}

function getRelationship(str, lineNum){
    if (str==="}->"){
        return "manyToOne";
    }
    else if (str==="<-{"){
        return "oneToMany";
    }
    else if (str==="<->"){
        return "oneToOne";
    }
    else if ("}-{"){
        return "manyToMany";
    }
    else{
        throw Error(`Invalid relationship format on line ${lineNum}`);
    }
}

function revType(str){
    if (str==="manyToOne"){
        return "oneToMany";
    }
    else if (str==="oneToMany"){
        return "manyToOne";
    }
    else {
        return str;
    }
}

function defineRelationships(string, collectionByName){
    let lines = string.split("\n");
    let i = 0;
    for (let line of lines){
    	if (line.trim()===""){
      	lines.splice(i,1);
      }
      i++;
    }
    let lastCollection = collectionByName[lines[0].trim()];
    let colStack = [];
    lines.shift();
    i = 2;
    let lastLen = 0;
    let sep = lines[1].slice(0, lines[1].indexOf(lines[1].trim().slice(0,3)));
    let rel, firstColon, secondColon, collectionLabel, revCollectionLabel, collection, len;
    let n = 0;
    for (let line of lines){
        len = line.split(sep).length;
        if (len > lastLen){
            //push last col on stack
            colStack.push(lastCollection)
            collection = lastCollection;
        }
        else if (len===lastLen){
            //do nothing
        }
        else{
            //pop off n
            n = lastLen - len;
            for (var j=0;j<n;j++){
                colStack.pop();
                collection = colStack[colStack.length-1]
            }
        }
        lastLen = len;
        line=line.trim();
        rel = getRelationship(line.slice(0,3), i);
        revCollection = collectionByName[line.slice(3,line.indexOf(" "))]

        firstColon = line.indexOf(":"); secondColon = line.slice(firstColon+1,Infinity).indexOf(":")+firstColon+1;
        collectionLabel = line.slice(firstColon+1, secondColon).trim();
        revCollectionLabel = line.slice(secondColon+1, Infinity).trim();

        collection.associations[collectionLabel] = {
            collection: revCollection.name,
            inverse: revCollectionLabel,
            type: rel
        }
        revCollection.associations[revCollectionLabel] = {
            collection: collection.name,
            inverse: collectionLabel,
            type: revType(rel)
        }
        lastCollection = revCollection;
        i++;
    }
}


class Database{
    /**
     * Uses apater to ensure collections and create database
     * @param {Adapter} adapter 
     * @param {Array<Collection>} collections 
     * @returns {Promise<Database>}  
     */
    constructor(config){
        this.createLookupObject = Database.createLookupObject;
        this.toAndOr = Database.toAndOr;
        if (config.adapter===undefined){
            throw Error("Adapter is required");
        }
        if (config.collections===undefined){
            throw Error("Collections are required");
        }
        var adapter = config.adapter;
        this.adapter = adapter;
        this.collections = [];
        this.callbacks = {};
        let scope = this;
        let collection;
        return async function(){
            //Grab collections from folder
            if (typeof config.collections === "string"){
                let collections = [];
                for (let filename of (await listFiles(config.collections))){
                    collections.push(require(path.join(config.collections, filename)));
                }
                config.collections = collections;
            }
            if (config.collections instanceof Object){
                Object.keys(config.collections).map((collectionName)=>{
                    config.collections[collectionName] = {
                        name: collectionName,
                        attributes: config.collections[collectionName],
                        associations: {}
                    }
                })
                console.log(config.collections)
                if (config.structure){
                    defineRelationships(config.structure, config.collections)
                }
                console.log(config.collections)
                config.collections = Object.keys(config.collections).map((collectionName)=>{
                    return {
                        model: {
                            name: collectionName,
                            attributes: config.collections[collectionName].attributes,
                            associations: config.collections[collectionName].associations
                        }
                    }
                })
            }
            await Promise.all(config.collections.map((collection)=>{
                return scope.ensureCollection(collection);
            }));
            scope.collections.push(...config.collections);
            if (config.fake!==undefined){
                console.log("generating fake records");
                await scope.generateFakeRecords();
            }
            return scope;
        }();
    }
    async ensureCollection(collection){
        return await this.adapter.ensureCollection(collection);
    }
    async query(jsoq, item){
        console.log(jsoq);
        let ast = new JSOQ(jsoq).getAst();
        if (ast.name==="assignment"){
            if (ast.children[1]===undefined){
                return await this.update(Database.createLookupObject(ast.children[0]), item);
            }
            else if (ast.children[1].name==="validJSON"){
                return await this.update(Database.createLookupObject(ast.children[0]), JSON.parse(ast.children[1].value));
            }
            else{
                return await this.updateToLookup(Database.createLookupObject(ast.children[0]), Database.createLookupObject(ast.children[1]));
            }
        }
        else if (ast.name=="insert"){
            if (ast.children[1]==undefined){
                return await this.insert(Database.createLookupObject(ast.children[0]), item);
            }
            else if (ast.children[1].name==="validJSON"){
                return await this.insert(Database.createLookupObject(ast.children[0]), JSON.parse(ast.children[1].value));
            }
            else{
                return await this.insertFromLookup(Database.createLookupObject(ast.children[0]), Database.createLookupObject(ast.children[1]));
            }
        }
        else{
            return await this.find(Database.createLookupObject(ast));
        }
    }
    static createLookupObject(ast){
        let lookup = {
            path: [],
            pipes: [],
            fields: []
        }
        let path;
        if (ast.name==="lookup"){
            path=ast.children[0];
        }
        else{
            path = ast;
        }
        let pathChildren = chunkArray(path.children, 2)
        for (let lu of pathChildren){
            let nextLookup = {};
            nextLookup.collection = lu[0].value;
            if (lu[1]!==undefined){
                for (let child of lu[1].children){
                    if (child.name==="queryAnd"){
                        nextLookup.filters = this.toAndOr(child);
                    }
                    if (child.name==="pipes"){
                        nextLookup.pipes = [];
                        for (let pipe of child.children){
                            nextLookup.pipes.push({
                                name: pipe.children[0].value,
                                argument: pipe.children[1] ? JSON.parse(pipe.children[1].value) : undefined
                            });
                        }
                    }
                }    
            }
            lookup.path.push(nextLookup);
        }
        let rest = ast.children.slice(1,ast.children.length);
        for (let next of rest){
            if (next.name==="queryAnd"){
                lookup.path[lookup.path.length-1].filters = this.toAndOr(next);
            }
            if (next.name==="pipes"){
                let pipes = [];
                for (let pipe of next.children){
                    pipes.push({
                        name: pipe.children[0].value,
                        argument: pipe.children[1] ? JSON.parse(pipe.children[1].value) : undefined
                    });
                }
                lookup.path[lookup.path.length-1].pipes = pipes;
            }
            if (next.name==="fields"){
                let fields = next;
                let scope = this;
                let processFields = function(fields){
                    let fieldsArr = []
                    for (let field of fields.children){
                        let fieldObj = {attribute: "", fields: []};
                        if (field.children.length===1){
                            //lookup.fields.push(field.children[0].value);
                            fieldObj.attribute = field.children[0].value;
                        }
                        else{
                            fieldObj.attribute = field.children[0].value;
                            if (field.children[1].name==="fields"){
                                fieldObj.fields.push(...processFields(field.children[1]));
                            }
                            else{
                                fieldObj.filters = scope.toAndOr(field.children[1]);
                                fieldObj.fields = processFields(field.children[2])
                            }
                        }
                        if (fieldObj.attribute!==""){
                            fieldsArr.push(fieldObj);
                        }
                    }
                    return fieldsArr;
                }
                lookup.fields = processFields(fields);
            }
        }
        return lookup;
        /**
        {
            lookup: [{
                name: "string",
                filters: {
                    or: [filter],
                    and: [filter],
                    attribute: "string",
                    comparison: "string",
                    value: "any"
                }
            }],
            pipes: [{
                name:,
                argument:
            }],
            fields: [{
                attribute: "string",
                filters: {},
                fields: ["fields"]
            }]
        }
        */
    }
    async find(query){
        return await this.adapter.find(query, this);
    }
    async insert(query, item){
        return await this.adapter.insert(query, item, this);
    }
    async update(query){
        return await this.adapter.update(query, this);
    }
    async delete(query){
        return await this.adapter.delete(query, this);
    }
    getCollectionByName(name){
        let cols = this.collections.filter((collection)=>{
            return (collection.model.name===name);
        });
        if (cols.length===1){
            return cols[0]
        }
        if (cols.length > 1){
            throw Error("Two collections have same name: "+name);
        }
        else{
            throw Error("Collection '"+name+"' not found");
        }
    }
    static toAndOr(andOr){
        if (andOr.name==="queryRule"){
            return {
                attribute: andOr.children[0].value,
                comparison: andOr.children[1].value,
                value: JSON.parse(andOr.children[2].value)
            }
        }
        else{
            if (andOr.children.length===1){
                return this.toAndOr(andOr.children[0])
            }
            if (andOr.name==="queryAnd"){
                return {and: andOr.children.map((child)=>this.toAndOr(child))};
            }
            if (andOr.name==="queryOr"){
                return {or: andOr.children.map((child)=>this.toAndOr(child))};
            }
        }
    }
    getCollectionFromAssociation(collection, associationName){

    }
}

/**
 * Returns an array with arrays of the given size.
 *
 * @param myArray {Array} array to split
 * @param chunk_size {Integer} Size of every group
 */
function chunkArray(myArray, chunk_size){
    var index = 0;
    var arrayLength = myArray.length;
    var tempArray = [];
    
    for (index = 0; index < arrayLength; index += chunk_size) {
        myChunk = myArray.slice(index, index+chunk_size);
        // Do something if you want with the group
        tempArray.push(myChunk);
    }

    return tempArray;
}

let exists = Symbol("Asserts existance of record")
let upsert = Symbol("Attempt to match, if it doesn't exist insert")

Database.exists = exists;
Database.upsert = upsert;

module.exports = Database;