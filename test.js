module.exports = async function({Adapter, JsoqsDb, describe, it, assert, faker}){
    let structure = `
    article
        }->journal : journal : articles
        }-{author : authors : articles
        }-{article : references : references
    `;

    let collections = {
        article: {
            name: {type: "string", unique: true}
        },
        journal: {},
        author: {}
    };

    let author1 = {
        name: faker.name.firstName()+faker.name.lastName()
    }
    let authors = []
    for (let i=0;i<10;i++){
        authors.push({
            name: faker.name.firstName()+faker.name.lastName()
        })
    }
    let articles = []
    for (let i=0;i<10;i++){
        articles.push({
            name: faker.lorem.words()
        })
    }
    let article1 = {
        name: faker.lorem.words(),
        authors: [author1, ...authors.slice(0,4)],
        references: articles.slice(0,4)
    }
    let article2 = {
        name: faker.lorem.words(),
        authors: [author1, ...authors.slice(0,4)],
        references: articles.slice(0,2).concat([article1])
    }

    let assertSameObject = (ob1,obj2)=>{return JSON.stringify(obj1)===JSON.stringify(obj2)}
    let db = await new JsoqsDb({
        adapter: Adapter,
        structure: structure,
        collections: collections
    });

    describe("Testing driver", function(){
        it("inserts a record", async function(){
            let mu = await db.query("article<=",article1);
            assert(mu.article.inserts.length===1);
        })
        it("inserts a matched record with exists", async function(){
            let mu = await db.query("article<=",{
                name: faker.lorem.words(),
                authors: [author1, ...authors.slice(0,4)],
                references: articles.slice(0,2).concat([{
                    id: JsoqsDb.exists,
                    ...article1
                }])
            });
            assert(mu.article.inserts.length===1);
        })
        it("inserts a matched record with upsert", async function(){
            let mu = await db.query("article<=",{
                id: JsoqsDb.upsert,
                name: faker.lorem.words(),
                authors: [author1, ...authors.slice(0,4)],
                references: articles.slice(0,2).concat([{
                    id: JsoqsDb.upsert,
                    ...article1
                }])
            });
            assert(mu.article.inserts.length===1);
        })
        it("inserts a matched record with id", async function(){
            let mu = await db.query("article<=",{
                name: faker.lorem.words(),
                authors: [author1, ...authors.slice(0,4)],
                references: articles.slice(0,2).concat([{
                    id: JsoqsDb.exists,
                    ...article1
                }])
            });
            assert(mu.article.inserts.length===1);
        })
        it("batch inserts records", async function(){
            let mu = await db.query("article<=",articles);
            assert(mu.article.inserts.length===50);
        })
        it("running assiments", async function(){
            await db.query(`author[name="${author1.name}"].age=54`)
            await db.query(`author[name="${author1.name}"]=`,{favoriteFood: "pizza"})
            let res = (await db.query(`author[name="${author1.name}"]{age, favoriteFood}`))[0];
            assertSameObject(res, {age: 54, favoriteFood: "pizza"})
        })
        it("runnning find queries", async function(){
            /*
            article[title="The History Of Programming Languages"]{authors,journal,references}
            author[name="Professor Stein"]{name,age,favoriteFood,articles{title}}

            author[name="Professor Stein"].articles

            author[name="Professor Stein"].articles[title="The History Of Programming Languages"]
            */
        })
        it("runnning aggregation queries", async function(){
            /*
            author[name="Professor Stein"].articles<limit: 10,skip: 10, count: "numberOfArticles">
            article[title="The History Of Programming Languages"].authors<sum: {field: "age", as: "totalAge"}>

            article[title="The History Of Programming Languages"].authors<avg: {field: "age", as: "averageAge"}>

            article[title="The History Of Programming Languages"].authors<min: {field: "age", as: "minumumAge"}>

            article[title="The History Of Programming Languages"].authors<max: {field: "age", as: "maximumAge"}>
            */
        })
        it("runnning delete queries", async function(){
            
        })
    })
}